[InstallShield Silent]
Version=v5.00.000
File=Response File
[File Transfer]
OverwriteReadOnly=NoToAll
[DlgOrder]
Dlg0=SdWelcome-0
Count=8
Dlg1=SdLicense-0
Dlg2=SdAskDestPath-0
Dlg3=SdSetupTypeEx-0
Dlg4=SdComponentDialog2-0
Dlg5=AskYesNo-0
Dlg6=SdStartCopy-0
Dlg7=SdFinish-0
[SdWelcome-0]
Result=1
[SdLicense-0]
Result=1
[SdAskDestPath-0]
szDir=C:\Program Files\Ligos\Indeo
Result=1
[SdSetupTypeEx-0]
Result=Custom
[SdComponentDialog2-0]
Indeo Audio Codec-type=string
Indeo Audio Codec-count=1
Indeo Audio Codec-0=Indeo Audio Codec\Indeo Audio Encoder
Component-type=string
Component-count=12
Component-0=Indeo Video 5 Quick Compressors
Component-1=Indeo® Video 5 Codec
Component-2=Indeo Video 4 Codec
Component-3=Indeo Video 3.2 Codec
Component-4=Indeo Audio Codec
Component-5=Indeo Video Raw (YVU9) Codec
Component-6=Indeo Video 4 Quick Compressors
Component-7=Indeo Video 5 Compressor Help Files
Component-8=Indeo Video 4 Compressor Help Files
Component-9=Indeo Software Release Notes
Component-10=Indeo Software Installation Source Code
Component-11=Indeo Software Uninstallation
Result=1
[AskYesNo-0]
Result=0
[SdStartCopy-0]
Result=1
[Application]
Name=Indeo® Software
Version=1.00.000
Company=Ligos
Lang=0009
[SdFinish-0]
Result=1
bOpt1=0
bOpt2=0